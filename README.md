The Perfect Sound is a simple iPhone app that plays sound effects.

It was available on the App Store for a year, but I chose not to continue
updating the project or paying for an App Store account.

This source is licensed under the MIT License.

Copyright (c) 2011 Robert Roland

