//
//  SoundPlayerViewController.m
//  sadtrombone
//
//  Created by Robert Roland on 4/30/11.
//  Copyright 2011 Robert Roland. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a 
//  copy of this software and associated documentation files (the "Software"), 
//  to deal in the Software without restriction, including without limitation 
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//  and/or sell copies of the Software, and to permit persons to whom the 
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included 
//  in all copies or substantial portions of the Software.
// 
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "SoundPlayerViewController.h"
#import "SoundEntry.h"

@implementation SoundPlayerViewController

- (IBAction)soundTheTrombone:(id)sender
{
    NSLog(@"Sounding the trombone!");
    
    if([audioPlayer isPlaying]) {
        [audioPlayer stop];
    } else {
        [audioPlayer setCurrentTime:0];
        [audioPlayer play];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    if (self) {        
        UITabBarItem *tbi = [self tabBarItem];        
        [tbi setTitle:@"The Perfect Sound"];
        
        UIImage *i = [UIImage imageNamed:@"Trombone-icon.png"];
        [tbi setImage:i];
    }
    
    return self;
}

- (void)dealloc
{
    [audioPlayer release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self view] setBackgroundColor:[UIColor blackColor]];
    
    // initialize the sound array
    
    soundArray = [[NSMutableArray alloc] init];
    
    [self addSound:@"Sad_Trombone" withLabel:@"Sad Trombone" toArray:soundArray];
    [self addSound:@"Rimshot" withLabel:@"Rimshot" toArray:soundArray];
    [self addSound:@"Carwash_Buzzer" withLabel:@"Carwash Buzzer" toArray:soundArray];
    [self addSound:@"Missile" withLabel:@"Missile" toArray:soundArray];
    [self addSound:@"Grenade" withLabel:@"Grenade" toArray:soundArray];
    [self addSound:@"Transporter" withLabel:@"Transporter" toArray:soundArray];
    
    [pickerView selectRow:0 inComponent:0 animated:NO];
    [self selectSound:0];
}

- (void)addSound:(NSString *)sound withLabel:(NSString *)label toArray:(NSMutableArray *)array
{
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:sound
                                                          ofType:@"m4a"];
    
    if (soundPath) {
        NSURL *soundUrl = [NSURL fileURLWithPath:soundPath];
        
        SoundEntry *entry = [[SoundEntry alloc] init];
        
        [entry setSoundLabel:label];
        [entry setSoundLocation:soundUrl];
        [entry setSoundName:sound];
        
        [array addObject:entry];
        
        // we can release our entry as it is retained by the array
        [entry release];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

// UIPickerView delegate functions

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self selectSound:row];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [soundArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[soundArray objectAtIndex:row] soundLabel];
}

// helper methods

- (void)selectSound:(NSInteger)soundIndex
{
    SoundEntry *entry = [soundArray objectAtIndex:soundIndex];
    
    NSLog(@"You selected a row in the picker: %@", [entry soundName]);
    
    if (audioPlayer) {
        [audioPlayer release];
    }
    
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[entry soundLocation] error:nil];    
}

@end
